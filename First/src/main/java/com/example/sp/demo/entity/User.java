package com.example.sp.demo.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

// userテーブルのエンティティ
@Entity
@Table(name="user")
public class User {
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name = "id" ,nullable = false)
	private Integer id;

	@Column(name = "loginId" ,nullable = false,unique=true)
	private String loginId;

	@Column(name = "name" ,nullable = false)
	private String name;

	@Column(name = "birthDate" ,nullable = false)

	private Date birthDate;

	@Column(name = "password" ,nullable = false)
	private String password;

	@Column(name = "createDate" ,nullable = false)
	private Date createDate;

	@Column(name = "updateDate" ,nullable = false)
	private Date updateDate;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getLoginId() {
		return loginId;
	}

	public void setLoginId(String login_id) {
		this.loginId = login_id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Date getBirthDate() {
		return birthDate;
	}

	public void setBirthDate(Date birth_date) {
		this.birthDate = birth_date;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {

		this.password = password;
	}

	public Date getCreateDate() {
		return createDate;
	}

	public void setCreateDate(Date create_date) {
		this.createDate = create_date;
	}

	public Date getUpdateDate() {
		return updateDate;
	}

	public void setUpdateDate(Date update_date) {
		this.updateDate = update_date;
	}





}

