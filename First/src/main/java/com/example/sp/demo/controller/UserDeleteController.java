package com.example.sp.demo.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.example.sp.demo.entity.SessionBean;
import com.example.sp.demo.entity.User;
import com.example.sp.demo.entity.UserRepository;
import com.example.sp.demo.form.Updateform;

@Controller
@RequestMapping("/UserDelete")
public class UserDeleteController {
	@Autowired
	UserRepository userRepo;

	@Autowired
	protected SessionBean session;

	@ModelAttribute("userform")
    public Updateform setupForm() {
         Updateform userform = new Updateform();
        return userform;

	}

	int Id;

	@RequestMapping(value = "DeleteCheck", method = RequestMethod.GET)
	public ModelAndView UserDelteCheck(@ModelAttribute Updateform userform, ModelAndView mav) {

		//ログインセッションがない場合、ユーザ一覧画面にリダイレクトさせる
		Integer sessioncheck = session.getId();

		if(sessioncheck==null) {
			mav.setViewName("redirect:/Index/");
			return mav;
		}

		//表示するログイン中のユーザー情報をセット
		String loginUser = session.getLoginId();
		mav.addObject("loginUser",loginUser);

		//URLから取得したIDを引数に削除したいユーザーを検索し取得。
		Id = userform.getId();
		User DeleteUser = userRepo.findById(Id).get();

		//情報をセット
		mav.addObject("DeleteUser", DeleteUser);



		mav.setViewName("SPuserDelete");
		return mav;
	}

	@RequestMapping(value = "Delete", method = RequestMethod.POST)
	public ModelAndView UserDelte(@ModelAttribute Updateform userform, ModelAndView mav) {

		// 削除処理
		userRepo.deleteById(Id);

		mav.setViewName("redirect:/UserList/");
		return mav;
	}
}
