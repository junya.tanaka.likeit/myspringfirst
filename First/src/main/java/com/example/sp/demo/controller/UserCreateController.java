package com.example.sp.demo.controller;

import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.example.sp.demo.entity.SessionBean;
import com.example.sp.demo.entity.User;
import com.example.sp.demo.entity.UserRepository;
import com.example.sp.demo.entity.UserService;
import com.example.sp.demo.form.Createform;

@Controller
@RequestMapping("/UserCreate")
public class UserCreateController {
	@Autowired
	UserRepository userRepo;

	@Autowired
	protected SessionBean session;

	@ModelAttribute("Createform")
    public Createform setupForm() {
         Createform form = new Createform();
        return form;
	}

	@RequestMapping(value = "Create", method = RequestMethod.GET)
	public ModelAndView UserCreate(@ModelAttribute Createform form, ModelAndView mav) {

		//ログインセッションがない場合、ユーザ一覧画面にリダイレクトさせる
		Integer sessioncheck = session.getId();

		if(sessioncheck==null) {
			mav.setViewName("redirect:/Index/");
			return mav;
		}

		//表示するログイン中のユーザー情報をセット
				String loginUser = session.getLoginId();
				mav.addObject("loginUser",loginUser);

		mav.setViewName("SPuserCreate");
		return mav;
	}

	@RequestMapping(value = "Add", method = RequestMethod.POST)
	public ModelAndView UserAdd(@ModelAttribute Createform form, ModelAndView mav,BindingResult result) {
				//エラーメッセージのバリデーション
				if (result.hasErrors()) {
					return UserCreate(form,mav);
				}
				//ログインIDの衝突チェック
				User LIDcheck = userRepo.findByLoginId(form.getLoginId());
				if(!(LIDcheck==null)) {
					mav.addObject("errmsg" , "エラーがあります。修正してください。");
					return UserCreate(form,mav);
				}

				//取得した情報をセット
				User user = new User();
				user.setLoginId(form.getLoginId());
				String md5Password = UserService.passChange(form.getPassword());
				user.setPassword(md5Password);
				user.setName(form.getName());
				user.setBirthDate(form.getBirthDate());
				user.setCreateDate(new Date());
				user.setUpdateDate(new Date());

				// 登録
				userRepo.save(user);

		mav.setViewName("redirect:/UserList/");
		return mav;
	}

}
