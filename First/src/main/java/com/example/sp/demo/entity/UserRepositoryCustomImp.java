package com.example.sp.demo.entity;

import java.util.Date;
import java.util.List;

import javax.persistence.EntityManager;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class UserRepositoryCustomImp implements UserRepositoryCustom {
	//https://dev.classmethod.jp/server-side/java/spring-data-jpa_search/

	@Autowired
	  EntityManager manager;

	@SuppressWarnings("unchecked")
	@Override
	public List<User> findsearch(String loginIdP, String nameP, Date birthdayStartP, Date birthdayEndP) {

			StringBuilder sql = new StringBuilder();

	    sql.append("SELECT login_id,name,birth_date FROM user where id != 1 ");
	    boolean andFlg = false;
	    boolean loginIdFlg = false;
	    boolean startDayFlg = false;
	    boolean endDayFlg = false;
	    if (!"".equals(loginIdP) && loginIdP != null) {
	        sql.append("and login_id =" +"'"+loginIdP+"'");
	        loginIdFlg = true;
	        andFlg = true;
	      }
	    boolean nameFlg = false;
	    if (!"".equals(nameP) && nameP != null) {
	      if (andFlg) sql.append(" AND ");
	      sql.append("name LIKE :nameP ");
	      nameFlg = true;
	      andFlg = true;
	    }
	    if (birthdayStartP != null) {
	      if (andFlg) sql.append(" AND ");
	      sql.append("birthDate >= :birthdayStartP ");
	      startDayFlg = true;
	      andFlg = true;
	    }
	    if (birthdayEndP != null) {
	      if (andFlg) sql.append(" AND ");
	      sql.append("birthDate <= :birthdayEndP ");
	      endDayFlg = true;
	      andFlg = true;
	    }
	    javax.persistence.Query query = manager.createNativeQuery(sql.toString());
	    if (loginIdFlg) query.setParameter("loginIdP", loginIdP);
	    if (nameFlg) query.setParameter("nameP", "%" + nameP + "%");
	    if (startDayFlg) query.setParameter("birthdayStartP", birthdayStartP);
	    if (endDayFlg) query.setParameter("birthdayEndP",birthdayEndP);


		return query.getResultList();
	  }

	}


