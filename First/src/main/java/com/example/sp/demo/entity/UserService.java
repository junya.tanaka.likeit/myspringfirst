package com.example.sp.demo.entity;

import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Date;
import java.util.List;

import javax.xml.bind.DatatypeConverter;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UserService {
	//1
	@Autowired
	private UserRepository userRepo;
	@Autowired
	private UserRepositoryCustom userRepoC;

	public List<User> findsearch(String loginIdP, String nameP, Date birthdayStartP, Date birthdayEndP){
		List<User> result;
		result = userRepoC.findsearch(loginIdP,nameP,birthdayStartP,birthdayEndP);
		return result;

	}


	//暗号化
	public static String passChange(String source)  {
	//ハッシュを生成したい元の文字列
		String result = null;


	//ハッシュ生成前にバイト配列に置き換える際のCharset
		Charset charset = StandardCharsets.UTF_8;
	//ハッシュアルゴリズム
		String algorithm = "MD5";

	//ハッシュ生成処理

		try {
			byte[] bytes = null;
			bytes = MessageDigest.getInstance(algorithm).digest(source.getBytes(charset));
			result = DatatypeConverter.printHexBinary(bytes);
		} catch (NoSuchAlgorithmException e) {

			// TODO 自動生成された catch ブロック
			e.printStackTrace();

		}return  result;
	}
}
