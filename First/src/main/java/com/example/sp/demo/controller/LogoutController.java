package com.example.sp.demo.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.example.sp.demo.entity.SessionBean;
import com.example.sp.demo.entity.UserRepository;
import com.example.sp.demo.form.Updateform;

@Controller
@RequestMapping("/logout")
public class LogoutController {

	@Autowired
	UserRepository userRepo;

	@Autowired
	protected SessionBean session;


	@RequestMapping(value = "signout", method = RequestMethod.GET)
	public ModelAndView index(@ModelAttribute Updateform userform, ModelAndView mav) {




	// ログイン時に保存したセッション内のユーザ情報を削除
	session.setId(null);
	session.setLoginId(null);
	session.setName(null);


	// ログインのサーブレットにリダイレクト
	mav.setViewName("redirect:/Index/login");
	return mav;


}
}
