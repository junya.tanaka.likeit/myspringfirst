package com.example.sp.demo.controller;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.example.sp.demo.entity.SessionBean;
import com.example.sp.demo.entity.User;
import com.example.sp.demo.entity.UserRepository;
import com.example.sp.demo.entity.UserRepositoryCustom;
import com.example.sp.demo.form.Listform;
@ComponentScan
@Controller
@RequestMapping("/UserList")
public class UserListController {

	@Autowired
	UserRepository userRepo;

	@Autowired
	UserRepositoryCustom userRepoCs;





	@Autowired
	protected SessionBean session;

	@ModelAttribute("Listform")
    public Listform setupForm() {
         Listform listform = new Listform();
        return listform;
	}
	@RequestMapping(value = "/", method = RequestMethod.GET)
	public ModelAndView UserList(@ModelAttribute Listform listform, ModelAndView mav) {

		//ログインセッションがない場合、ユーザ一覧画面にリダイレクトさせる
		Integer sessioncheck = session.getId();

		if(sessioncheck==null) {
			mav.setViewName("redirect:/Index/");
			return mav;
		}
		//ユーザーリスト全件取得してリストをセット
		List<User> userlist = userRepo.findAll();
		//管理者を除外
		userlist.remove(0);
		mav.addObject("userlist",userlist);

		//表示するログイン中のユーザー情報をセット
		String loginUser = session.getLoginId();
		mav.addObject("loginUser",loginUser);



		mav.setViewName("SPuserList");
		return mav;
	}
	@RequestMapping(value = "Search", method = RequestMethod.POST)
	public ModelAndView Search
	(@ModelAttribute Listform listform, ModelAndView mav,BindingResult result) {


		String loginIdP = listform.getLoginId();
		String nameP = listform.getName();
		Date startDateP = listform.getStartDate();
		Date endDateP = listform.getEndDate();


		//検索
		List<User> uList = userRepoCs.findsearch
				(loginIdP, nameP, startDateP, endDateP);




		mav.addObject("userlist",uList);

		mav.setViewName("SPuserList");
		return mav;
	}

}
