package com.example.sp.demo.form;
import java.io.Serializable;
import java.util.Date;

import javax.validation.constraints.AssertTrue;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

import org.springframework.format.annotation.DateTimeFormat;

import lombok.Data;
@Data
public class Createform implements Serializable{
	private int id;
	@NotEmpty(message = "入力されていない欄があります。")
	private String loginId;
	@NotEmpty(message = "入力されていない欄があります。")
	private String name;
	@NotNull(message = "入力されていない欄があります。")
	@DateTimeFormat(pattern = "yyyy-MM-dd")
	private Date birthDate;
	@NotEmpty(message = "入力されていない欄があります。")
	private String password;
	@NotEmpty(message = "入力されていない欄があります。")
	private String passwordChecker;

	private Date createDate;



	@AssertTrue(message="入力されたパスワードを確認してください。")
    public boolean isPasswordValid() {
		if(password == null) {
			return false;
		}else if (passwordChecker == null) {
			return false;
		}else if (password.equals(passwordChecker))
        	return true;
        else{
        return false;
        }
	}


	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getLoginId() {
		return loginId;
	}
	public void setLoginId(String loginId) {
		this.loginId = loginId;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public Date getBirthDate() {
		return birthDate;
	}
	public void setBirthDate(Date birthDate) {
		this.birthDate = birthDate;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getPasswordChecker() {
		return passwordChecker;
	}
	public void setPasswordChecker(String passwordchecker) {
		this.passwordChecker = passwordchecker;
	}

	public Date getCreateDate() {
		return createDate;
	}
	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}
}


