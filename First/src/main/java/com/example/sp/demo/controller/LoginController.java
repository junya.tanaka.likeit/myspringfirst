package com.example.sp.demo.controller;



import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.example.sp.demo.entity.SessionBean;
import com.example.sp.demo.entity.User;
import com.example.sp.demo.entity.UserRepository;
import com.example.sp.demo.entity.UserService;
import com.example.sp.demo.form.Loginform;



@Controller
@RequestMapping("/Index")
public class LoginController {

	@Autowired
	UserRepository userRepo;

	@Autowired
	protected SessionBean session;

	@ModelAttribute("Loginform")
    public Loginform setupForm() {
         Loginform loginform = new Loginform();
        return loginform;
	}



	@RequestMapping(value = "login", method = RequestMethod.GET)
	public ModelAndView index(@ModelAttribute Loginform loginform, ModelAndView mav) {
		//ログインセッションがある場合、ユーザ一覧画面にリダイレクトさせる
		Integer sessioncheck = session.getId();

		if(sessioncheck!=null) {
			mav.setViewName("redirect:/UserList/");
			return mav;
		}

		mav.setViewName("SPindex");
		return mav;
		}


	@RequestMapping(value = "login", method = RequestMethod.POST)
	public ModelAndView login(
			@Validated @ModelAttribute Loginform loginform,
			BindingResult result,
			ModelAndView mav,
			RedirectAttributes attributes
			) {

		//エラーメッセージのバリデーション
		if (result.hasErrors()) {
			return index(loginform,mav);
		}


		// 取得したパラメータを引数にユーザーを検索

		//暗号化
		String Cpassword = UserService.passChange(loginform.getPassword());
		User user = userRepo.findByLoginIdAndPassword(loginform.getLoginId(), Cpassword);

		// セッションにユーザの情報をセット
			session.setId(user.getId());
			session.setLoginId(user.getLoginId());
			session.setName(user.getName());



			mav.setViewName("redirect:/UserList/");
			return mav;

	}
}
