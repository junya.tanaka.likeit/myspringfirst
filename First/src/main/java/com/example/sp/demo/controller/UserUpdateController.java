package com.example.sp.demo.controller;

import java.util.Date;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.example.sp.demo.entity.SessionBean;
import com.example.sp.demo.entity.User;
import com.example.sp.demo.entity.UserRepository;
import com.example.sp.demo.entity.UserService;
import com.example.sp.demo.form.Updateform;

@Controller
@RequestMapping("/UserUpdate")
public class UserUpdateController {
	@Autowired
	UserRepository userRepo;

	@Autowired
	protected SessionBean session;


	int Id;

	@ModelAttribute("Updateform")
    public Updateform setupForm() {
         Updateform updateform = new Updateform();
        return updateform;
	}


	@RequestMapping(value = "Updateform", method = RequestMethod.GET)
	public ModelAndView UserUpdateForm(@ModelAttribute Updateform updateform, ModelAndView mav) {

		//ログインセッションがない場合、ユーザ一覧画面にリダイレクトさせる
				Integer sessioncheck = session.getId();

				if(sessioncheck==null) {
					mav.setViewName("redirect:/Index/");
					return mav;
				}


		//表示するログイン中のユーザー情報をセット
		String loginUser = session.getLoginId();
		mav.addObject("loginUser",loginUser);
		//idを引数に表示するユーザーの情報を取得。
		Id = updateform.getId();
		User UpdateUser = userRepo.findById(Id).get();

		//更新するユーザー情報をセット
		mav.addObject("UpdateUser", UpdateUser);

		mav.setViewName("SPuserUpdate.html");
		return mav;
	}

	@RequestMapping(value = "Update", method = RequestMethod.POST)
	public ModelAndView UserUpdate
	(@ModelAttribute("Updateform") @Valid Updateform updateform, BindingResult result, ModelAndView mav) {

		//エラーメッセージのバリデーション
		if (result.hasErrors()) {
			updateform.setId(Id);
			return UserUpdateForm(updateform,mav) ;
		}

		//idを引数に表示するユーザーの情報を取得。

		User UpdateUser = userRepo.findById(Id).get();

		//入力されたフォームをそれぞれにセットしアップデートを実行
		UpdateUser.setId(Id);
		UpdateUser.setName(updateform.getName());
		String md5Password = UserService.passChange(updateform.getPassword());
		UpdateUser.setPassword(md5Password);
		UpdateUser.setBirthDate(updateform.getBirthDate());
		UpdateUser.setUpdateDate(new Date());

		// 更新処理
		userRepo.save(UpdateUser);


		mav.setViewName("redirect:/UserList/");
		return mav;
	}
}
