package com.example.sp.demo.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.example.sp.demo.entity.SessionBean;
import com.example.sp.demo.entity.User;
import com.example.sp.demo.entity.UserRepository;
import com.example.sp.demo.form.Updateform;

@Controller
@RequestMapping("/UserDetail")
public class UserDetailController {
	@Autowired
	UserRepository userRepo;

	@Autowired
	protected SessionBean session;

	@ModelAttribute("userform")
    public Updateform setupForm() {
         Updateform form = new Updateform();
        return form;
	}

	@RequestMapping(value = "Detail", method = RequestMethod.GET)
	public ModelAndView UserDetail(@ModelAttribute Updateform userform, ModelAndView mav) {

		//ログインセッションがない場合、ユーザ一覧画面にリダイレクトさせる
				Integer sessioncheck = session.getId();

				if(sessioncheck==null) {
					mav.setViewName("redirect:/Index/");
					return mav;
				}

		//表示するログイン中のユーザー情報をセット
		String loginUser = session.getLoginId();
		mav.addObject("loginUser",loginUser);



		//idを引数に表示するユーザーの情報を取得。
		User user = userRepo.findById(userform.getId()).get();

		//情報をセット
		mav.addObject("user", user);

		mav.setViewName("SPuserDetail");
		return mav;
	}
}
