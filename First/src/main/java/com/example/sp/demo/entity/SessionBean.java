package com.example.sp.demo.entity;

import java.io.Serializable;

import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;
import org.springframework.stereotype.Component;

@Component
@Scope(value= "session", proxyMode = ScopedProxyMode.TARGET_CLASS)
public class SessionBean implements Serializable {

     private static final long serialVersionUID = 6334063099671792256L;

     private Integer id;

  	private String loginId;

  	private String name;



  	public Integer getId() {
  		return id;
  	}

  	public void setId(Integer id) {
  		this.id = id;
  	}

  	public String getLoginId() {
  		return loginId;
  	}

  	public void setLoginId(String login_id) {
  		this.loginId = login_id;
  	}

  	public String getName() {
  		return name;
  	}

  	public void setName(String name) {
  		this.name = name;
  	}






  }
